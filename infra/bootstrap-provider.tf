provider "google" {
  project = var.bootstrap_project
  region = var.gcp_region
  credentials = file(var.creds_svc_accnt_bootstrap_admin)
  version = "~> 3.39.0"
}
