output "gcp-dns_zone-org_root_zone" {
  description = "root hosted zone for org dns"
  value = google_dns_managed_zone.org_root_zone
}

output "gcp-dns_rec_set-gsuite_mx_records_in_root_zone" {
  description = "dns MX records for org gsuite"
  value = google_dns_record_set.org_gsuite_mx_records_in_root_zone
}

output "gcp-dns_rec_set-domain_verification_txt_records_in_root_zone" {
  description = "dns txt records for google webmaster domain verfication"
  value = google_dns_record_set.org_domain_verification_records_in_root_zone
}
