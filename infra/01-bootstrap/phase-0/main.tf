resource "google_organization_iam_member" "acnt_owner" {
  for_each = toset(var.acnt_owner_roles)
  org_id = var.org_id
  role = each.key
  # each.key and each.value are the same for set
  member = "user:${var.primary_account_owner_id}"
}

resource "google_organization_iam_member" "svc_acnt_bootstrap_admin" {
  for_each = toset(var.svc_acnt_bootstrap_admin_roles)
  org_id = var.org_id
  role = each.key
  # each.key and each.value are the same for set
  member = "serviceAccount:${var.bootstrap_svc_account_id}@${var.bootstrap_project_id}.iam.gserviceaccount.com"
}

resource "google_project_service" "bootstrap_project" {
  for_each = toset(var.bootstrap_project_services)
  depends_on = [
    google_organization_iam_member.svc_acnt_bootstrap_admin]
  project = var.bootstrap_project_id
  disable_dependent_services = true
  service = each.key
  # each.key and each.value are the same for set
}
