#!/usr/bin/env bash

set -o errexit  # always exit on error
set -o pipefail # do not ignore exit codes when piping output
set -o nounset  # exit if unset variables are used

source "${infra_compute_root}"/scripts/lib/echo.sh

echoInfo "installing addon: kubed"
echoInfo "creating kube-addons namespace"
kubectl create namespace kube-addons --dry-run=client -o yaml | kubectl apply -f -

helm repo add appscode https://charts.appscode.com/stable/
helm repo update

helm upgrade --install kubed appscode/kubed -n kube-addons --version v0.12.0 \
  --dry-run="${dry_run}" --debug="${debug}" --wait
