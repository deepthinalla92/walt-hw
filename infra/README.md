# infra

Infrastructure should be managed by code. This repo has the code that is needed to bootstrap a brand new GCP account and create necessary infrastructure to run multiple app environments powered by GKE.

The infra is created in three stages

1. [manual-steps](#manual-steps)
1. [01-bootstrap](#01-bootstrap)
2. [02-util-infra](#02-util-infra)
3. [03-compute](#03-compute)

### manual Steps

Before doing anything, the first step is to setup GCP account. Prerequisite for this is that you must have already set up your organization on GSuite.

1. Login to admin.google.com as the super admin
2. In a new tab navigate to console.cloud.google.com
3. Because, you are already authenticated as the super admin for your organization on GSuite, GCP will automatically populate your GSuite organization in the `projects` drop-down on the top left corner of the GCP Consol page.
4. Follow the instructions on [GCP Documentation](https://cloud.google.com/resource-manager/docs/creating-managing-organization#setting-up) to grant yourself as the `Organziation Administrator` on GCP.
5. Setup billing account
6. Make a note of the `organization id` from `IAM-> Manage Resources` section.

These below resources can not be created using terraform as we would first need at least a service account to set up the google terraform provider. 

Service accounts can not be created at organization level. So, we need to first create a project on GCP Console. 

Do the following on GCP Console to setup terraform environment on your local machine.

1. Create a project with name `account-bootstrap` in `IAM -> Manage Resource` section.
2. After the project is created, create a service account with name `bootstrap-admin` in `IAM / Service Accounts` by selecting `account-bootstrap` project from the projects drop-down.
3. Select the email of the `bootstrap-admin` service acount
3. Grant `Organization Admin` | `roles/resourcemanager.organizationAdmin` role to this new service account. 

  > Note: This should be done in IAM section by choosing the organization and not `account-bootstrap` project
  
4. Create and download the credentials key for the service account and store it at location specified in `creds_svc_accnt_bootstrap_admin` variable in [global-variables.tf](src/global-variables.tf) file
5. Enable Cloud Resource Manager API in `account-bootstrap` project

## 01-bootstrap

This stage has two phases. The second phase is dependent on the first phase.

### phase-0

1. Enable required google apis for `account-bootstrap` project to enable `svc_acnt_bootstap_admin` service account to create other resources
2. Grant required iam roles for account owner to access resources on console
3. Grant required iam roles for `svc_acnt_bootstap_admin` service account to create other resources

### phase-1

1. Create `util` project to put all the shared infra components like dns, tf-backend, artifact registry repos etc
2. Create resources to setup `terraform-backend`

## 02-util-infra

1. Create docker repo using the relatively new artifact registry service. This is an alternative to using GCR to host docker images.
2. Create DNS hosted zones for ingress domains for the applications

## 03-compute

Compute environment needed to run the services is created in this stage. This stage is primarily divided into two phases

### phase-1 - Deploy Cluster

This phase is further divided into two phases.

#### phase-1 - Env Infra

In this phase resources that are shared by multiple GKE clusters in the same environment, like google project, vpc network, NAT router etc
 
#### phase-2 - Cluster Infra

In this phase resource required to setup one GKE cluster are created. Separating shared infra from cluster infra provides the ability to 
easily create another GKE cluster by simply switching terraform workspace and modifying a few variables.

In this phase, a public ip address is also created, which is then supplied to nginx-ingress helm chart to set this IP as ingress load balancer IP. This step makes the IP that needs to be added to the DNS zones deterministic.

The public address created in the previous step is also added as the catch-all A record for all the requests coming to `*.stage.<domain>.com` and `*.prod.<domain>.com`.

So, all dns requests to  `*.stage.<domain>.com` and `*.prod.<domain>.com` resolve to the ingress load balancer(created when nginx-ingress help chart is installed) ip address.

### phase-2 - Deploy Addons

In this phase the below addons are installed using helm and kubectl.

1. ingress-nginx
2. cert-manager
3. kubed
4. one cert for `*.stage.<domain>.com` and `*.prod.<domain>.com` domains, which will be provisioned by letsencrypt.
