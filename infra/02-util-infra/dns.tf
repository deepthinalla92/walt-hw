resource "google_dns_managed_zone" "org_root_zone" {
  depends_on = [google_project_service.util_project]
  name = format("%s-root-zone", split(".", var.org_domain)[0])
  project = var.util_project_id
  dns_name = format("%s.", var.org_domain)
  description = "primary root dns zone for ${var.org_domain}"
  visibility = "public"
}

resource "google_dns_record_set" "org_gsuite_mx_records_in_root_zone" {
  name = format("%s.", var.org_domain)
  project = data.google_project.util.project_id
  managed_zone = google_dns_managed_zone.org_root_zone.name
  type = "MX"
  ttl = 3600

  rrdatas = var.org_gsuite_mx_records
}

resource "google_dns_record_set" "org_domain_verification_records_in_root_zone" {
  name = format("%s.", var.org_domain)
  project = data.google_project.util.project_id
  managed_zone = google_dns_managed_zone.org_root_zone.name
  type = "TXT"
  ttl = 3600

  rrdatas = var.org_google_domain_verification_txt_record
}

resource "google_dns_managed_zone" "k8sk8s_root_zone" {
  depends_on = [google_project_service.util_project]
  name = format("%s-root-zone", split(".", var.k8sk8s_domain)[0])
  project = var.util_project_id
  dns_name = format("%s.", var.k8sk8s_domain)
  description = "primary root dns zone for ${var.k8sk8s_domain}"
  visibility = "public"
}
