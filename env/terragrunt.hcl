locals  {
  svc_acnt_tf_bknd_creds = "/Users/deepthinalla/gdrive/deepthi/work/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
  tf_bknd_bucket = "deepthinalla-org-tf-bknd"
}

terraform {
    source = ".//bucket"

    extra_arguments "custom_vars" {
    commands = [
      "apply",
      "plan",
      "import",
      "push",
      "refresh"
    ]

    required_var_files = [
        "${get_terragrunt_dir()}/./common.tfvars",
    ]
    optional_var_files = [
        "${get_terragrunt_dir()}/./${get_env("TF_VAR_env","dev")}.tfvars",
    ]
    }
}

remote_state {
    backend = "gcs"
    generate = {
        path      = "backend.tf"
        if_exists = "overwrite"
    }
    config = {
        bucket         = local.tf_bknd_bucket
        prefix         = "${path_relative_to_include()}/${get_env("TF_VAR_env","dev")}/terraform.tfstate"
        credentials    = local.svc_acnt_tf_bknd_creds
    }
}

generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
provider "google" {
  project = var.bootstrap_project_id
  region = var.gcp_region
  credentials = file(var.creds_svc_accnt_bootstrap_admin)
  version = "~> 3.39.0"
}
EOF
}
