#!/usr/bin/env bash

set -o errexit  # always exit on error
set -o pipefail # do not ignore exit codes when piping output
set -o nounset  #exit if unset variables are used

export repo_root=${1}
export dry_run=${2}
export debug=${3}

export infra_compute_root=${repo_root}/infra/03-compute

source "${infra_compute_root}"/scripts/lib/kubeconfig.sh
source "${infra_compute_root}"/scripts/lib/echo.sh

echoInfo "deploying compute cluster addons"

echoInfo "trying to fetch cluster-info from the terraform state..."
pushd "${infra_compute_root}"/terraform/02-cluster-infra
rm -rf .terraform
terraform init
echoInfo "retrieving outputs from terraform state..."
ingress_lb_ip=$(terraform output -json | jq --raw-output '."gcp-ntwrk_ip_addr-env_ing_lb".value.address')
gke_cluster_name=$(terraform output -json | jq --raw-output '."gcp-gke_cluster-env_compute".value.name')
gke_cluster_endpoint=$(terraform output -json | jq --raw-output '."gcp-gke_cluster-env_compute".value.endpoint')
gke_cluster_master_ca_cert=$(terraform output -json | jq --raw-output '."gcp-gke_cluster-env_compute".value.master_auth[0].cluster_ca_certificate')
gke_cluster_node_count=$(terraform output -json gcp-gke_node_pool-env_compute_np_1 | jq --raw-output .node_count)
popd

if [ "${gke_cluster_endpoint}" != null ] && [ "${gke_cluster_node_count}" -gt 0 ]; then
  echoInfo "values retrieved from terraform output"
  echo "ingress_lb_ip: ${ingress_lb_ip}"
  echo "gke_cluster_name: ${gke_cluster_name}"
  echo "gke_cluster_endpoint: ${gke_cluster_endpoint}"
  set +o nounset
  if [[ -z "${GOOGLE_APPLICATION_CREDENTIALS}" ]]; then
    echoError "looks like GOOGLE_APPLICATION_CREDENTIALS is not exported."
    echoError "GOOGLE_APPLICATION_CREDENTIALS should be set to the path for service account creds file that has permissions to create kubeconfig for '${gke_cluster_name}' cluster"
    exit 1
  fi
  set -o nounset
  # The below script will generate a static kubeconfig file
  # GCP Auth plugin in the kubeconfig will use GOOGLE_APPLICATION_CREDENTIALS to fetch kubeconfig on the fly
  bash "${infra_compute_root}"/scripts/lib/kubeconfig.sh "${gke_cluster_name}" "${gke_cluster_endpoint}" "${gke_cluster_master_ca_cert}"

  export KUBECONFIG=$(pwd)/kubeconfig.yaml
  if [ "${debug}" = true ]; then
    echoInfo "generated static kubeconfig"
    cat "${KUBECONFIG}"
    echoInfo "GOOGLE_APPLICATION_CREDENTIALS: ${GOOGLE_APPLICATION_CREDENTIALS}"
  fi

  kubectl cluster-info

  [ "${dry_run}" == true ] && dry_run_server="server" || dry_run_server="none"

  source ${infra_compute_root}/scripts/deploy/addons/01_ingress-controller.sh "${ingress_lb_ip}"
  source ${infra_compute_root}/scripts/deploy/addons/01_kubed.sh
  source ${infra_compute_root}/scripts/deploy/addons/02_cert-manager.sh
  rm -f "${KUBECONFIG}"

else
  echoWarning "looks like cluster has not yet been created yet or there are no nodes attached to the cluster. skipping addons..."
fi
