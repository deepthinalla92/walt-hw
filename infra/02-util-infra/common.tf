data "google_project" "util" {
  # this will take the provider project-id
}

resource "google_project_service" "util_project" {
  for_each = toset(var.util_project_services)
  project = data.google_project.util.project_id
  disable_dependent_services = true
  service = each.key
  # each.key and each.value are the same for set
}
