#!/usr/bin/env bash

set -o errexit      # always exit on error
set -o pipefail     # do not ignore exit codes when piping output
set -o nounset      #exit if unset variables are used

export repo_root=${1}
export dry_run=${2}
export debug=${3}

export infra_compute_root=${repo_root}/infra/03-compute

source "${infra_compute_root}"/scripts/lib/echo.sh

source "${infra_compute_root}"/scripts/lib/kubeconfig.sh

echoInfo "running terraform for compute-clusters env-infra"
pushd "${infra_compute_root}"/terraform/01-env-infra
rm -rf .terraform
terraform init
terraform plan
if [ "${dry_run}" = false ]; then
  terraform apply --auto-approve
fi
popd

echoInfo "running terraform for compute-clusters cluster-infra"
pushd "${infra_compute_root}"/terraform/02-cluster-infra
rm -rf .terraform
terraform init
terraform plan
if [ "${dry_run}" = false ]; then
  terraform apply --auto-approve
fi
popd
