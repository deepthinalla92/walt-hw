#!/usr/bin/env bash

set -o errexit      # always exit on error
set -o pipefail     # do not ignore exit codes when piping output
set -o nounset      #exit if unset variables are used

export repo_root=${1}
export dry_run=${2}
export debug=${3}

compute_infra_tf_root=${repo_root}/infra/03-compute/terraform

echoInfo "running terraform destroy for env-infra"
pushd "${compute_infra_tf_root}"/01-env-infra
rm -rf .terraform
terraform init
terraform plan
terraform output
terraform plan -destroy
if [ "${dry_run}" = false ]; then
  terraform destroy --auto-approve
fi
popd
echoInfo "running terraform destroy for cluster-infra"
pushd "${compute_infra_tf_root}"/02-cluster-infra
rm -rf .terraform
terraform init
terraform output
terraform plan -destroy
if [ "${dry_run}" = false ]; then
  terraform destroy --auto-approve
fi
popd
