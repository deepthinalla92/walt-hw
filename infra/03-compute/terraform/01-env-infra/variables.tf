variable "k8s_external_secrets_namespace" {
  type = string
  default = "kube-addons"
}

variable "k8s_external_secret_KSA" {
  type = string
  default = "external-secrets-mgr"
}

variable "util_project_id" {
  type = string
  default = "util-ee07"
}

variable "compute_project_services" {
  type = list(string)
}
