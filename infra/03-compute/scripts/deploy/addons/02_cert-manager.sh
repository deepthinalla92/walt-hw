#!/usr/bin/env bash

set -o errexit  # always exit on error
set -o pipefail # do not ignore exit codes when piping output
set -o nounset  # exit if unset variables are used

source "${infra_compute_root}"/scripts/lib/echo.sh

echoInfo "installing addon: cert-manager"
echoInfo "creating cert-manager namespace"
kubectl create namespace cert-manager --dry-run=client -o yaml | kubectl apply -f -

echoInfo "creating secret for gcp svc acc that will be used by cert manager for solving dns challenges while issuing certs"
[ "${dry_run}" == true ] && dry_run_server=server || dry_run_server=none

pushd "${infra_compute_root}"/terraform/01-env-infra
terraform output -json gcp-svc_acc_key-dns_01_solver_key | jq -r .private_key | base64 -d >dns01-solver-gcp-svc-acc-key.json


kubectl create secret generic dns01-solver-gcp-svc-acc -n cert-manager \
  --from-file=dns01-solver-gcp-svc-acc-key.json --dry-run=true -o yaml | kubectl apply -f -

rm -f dns01-solver-gcp-svc-acc-key.json
popd

echoInfo "creating secrets for certificates of ingress domains"
kubectl apply -f "${infra_compute_root}"/manifests/cert-manager/01_cert_secrets.yaml --dry-run=${dry_run_server}

echoInfo "deploying 'cert-manager' addon using helm chart"
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.1/cert-manager.crds.yaml

helm repo add jetstack https://charts.jetstack.io
helm repo update

helm upgrade --install cert-manager --namespace cert-manager \
  --version v0.14.1 jetstack/cert-manager \
  --dry-run="${dry_run}" --debug="${debug}" --wait

echoInfo "creating a certificate issuer"
kubectl apply -f "${infra_compute_root}"/manifests/cert-manager/01_cluster_issuer.yaml --dry-run=${dry_run_server}

echoInfo "creating certificates for ingress domains"
kubectl apply -f "${infra_compute_root}"/manifests/cert-manager/02_certs.yaml --dry-run=${dry_run_server}
echoInfo "successfully installed cert-manager addon"
