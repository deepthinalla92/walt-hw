# Deepthi Nalla Submission

1. All the code and documentation for infra can be found in [infra/README.md](infra/README.md)
2. Helm Chart in charts/rlt-test has been modified to work with the app pipeline
3. The following can be observed as part of application build and deploy steps in [.gitlab-ci.yml](.gitlab-ci.yml) file

    a) Helm chart is packaged and published to a private helm repo set up using Google Storage Bucket and Helm GCS plugin in [gitlab pipeline jobs - helm_chart](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L116-136).
    
    b) App Docker image is built and pushed to Google Artifact Registry in [gitlab pipeline job - docker_image[app]](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L105-114). The docker image built steps are added [as yaml anchors in gitlab file](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L27-33).
    
    c) App is deployed to stage environment on commits to master and merge-requests only in [gitlab pipeline job - deploy_app[stage]](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L138-153).
    
    d) App is deployed to prod environment on commits to master with a manual gate in [gitlab pipeline job - deploy_app[prod]](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L155-168).
    
## What to expect

1. GKE cluster and addons can be created by running just one command.

   ```
   make deploy repo_root="/project/root" dry_run=true #dry-run
   make deploy repo_root="/project/root" dry_run=false
   ``` 

2. Every merge request creates a separate app environment. ex: for merge request #1 the app is accessible using rlt-test-mr-1.stage.k8sk8s.com

3. Because the app deployment logic is same for both stage and prod deployments, [the logic is encapsulated in an yaml anchor and is reused in the gitlab-ci file](https://gitlab.com/deepthinalla92/walt-hw/-/blob/master/.gitlab-ci.yml#L35-69).

4. Both stage and prod environments are created in the same cluster.

5. App deployed to stage from master branch can be accessed at https://rlt-test.stage.k8sk8s.com
    
6. App deployed to stage from a merge-request can be accessed at https://rlt-test-mr-<mr-id>.stage.k8sk8s.com. An example can be accessed at https://rlt-test-mr-2.stage.k8sk8s.com

7. App deployed to prod from master branch can be accessed at https://rlt-test.prod.k8sk8s.com.
