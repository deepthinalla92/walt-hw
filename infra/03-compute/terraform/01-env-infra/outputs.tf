output "gcp-prjct-compute" {
    description = "project for compute cluster"
    value = google_project.compute
}

output "gcp-nw_vpc-env_vpc" {
    description = "env vpc"
    value = google_compute_network.env_vpc
}

output "gcp-nw_sn-env_vpc_subnet" {
    description = "env vpc private subnet for workers"
    value = google_compute_subnetwork.env_vpc_subnet
}

//output "gcp-nw_router-env_router" {
//    description = "env network router to enable nat"
//    value = google_compute_router.env_router
//}
//
//output "gcp-nw_nat-env_router_nat" {
//    description = "nat router for workers in private subnet"
//    value = google_compute_router_nat.env_router_nat
//}

output "gcp-svc_acc-dns_01_solver" {
    description = "service acc with dns admin role in org_dns project to solve dns challenges while creating ssl certs"
    value = google_service_account.dns_01_solver
}

output "gcp-svc_acc_key-dns_01_solver_key" {
    description = "private key json for dns_01_solver service acc"
    value = google_service_account_key.dns_01_solver_key
    sensitive = true
}

output "gcp-project_iam_member-dns_01_solver_role_binding" {
    description = "dns admin role attached to dns_01_solver service acc in org_dns project"
    value = google_project_iam_member.dns_01_solver_role_binding
}
