provider "google-beta" {
  project = var.util_project_id
  region = var.gcp_region
  credentials = file(var.creds_svc_accnt_bootstrap_admin)
  version = "~> 3.39.0"
}

provider "google" {
  project = var.util_project_id
  region = var.gcp_region
  credentials = file(var.creds_svc_accnt_bootstrap_admin)
  version = "~> 3.39.0"
}
