resource "google_artifact_registry_repository" "org_docker_repo" {
  depends_on = [google_project_service.util_project]
  provider = google-beta
  location = var.gcp_region
  repository_id = var.org_docker_repo_name
  description = "org docker repo"
  format = "DOCKER"
  project = data.google_project.util.project_id
  # deleting this resource will delete all the docker images inside the repo. So the below block is added to prevent deletion of resource. Only remove the below block if you know what you are doing.
  lifecycle {
    prevent_destroy = true
  }
}

resource "google_service_account" "artifact_registry_reader" {
  provider = google-beta

  account_id = "artifact-registry-reader"
  display_name = "artifact registry service account with reader role"
  project = data.google_project.util.project_id
}

resource "google_artifact_registry_repository_iam_member" "artifact_registry_reader_docker_repo_iam" {
  provider = google-beta

  location = google_artifact_registry_repository.org_docker_repo.location
  repository = google_artifact_registry_repository.org_docker_repo.name
  role = "roles/artifactregistry.reader"
  member = "serviceAccount:${google_service_account.artifact_registry_reader.email}"
  project = data.google_project.util.project_id
}

resource "google_service_account" "artifact_registry_writer" {
  provider = google-beta

  account_id = "artifact-registry-writer"
  display_name = "artifact registry service account with writer role"
  project = data.google_project.util.project_id
}

resource "google_artifact_registry_repository_iam_member" "artifact_registry_writer_docker_repo_iam" {
  provider = google-beta

  location = google_artifact_registry_repository.org_docker_repo.location
  repository = google_artifact_registry_repository.org_docker_repo.name
  role = "roles/artifactregistry.writer"
  member = "serviceAccount:${google_service_account.artifact_registry_writer.email}"
  project = data.google_project.util.project_id
}

resource "google_service_account_key" "artifact_registry_reader_key" {
  service_account_id = google_service_account.artifact_registry_reader.name
}

resource "google_service_account_key" "artifact_registry_writer_key" {
  service_account_id = google_service_account.artifact_registry_writer.name
}

resource "google_storage_bucket" "helm_repo" {
  project = data.google_project.util.project_id
  name = "org-helm-repo"
  storage_class = "MULTI_REGIONAL"
  force_destroy = false
}
