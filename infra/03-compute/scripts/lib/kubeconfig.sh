#!/usr/bin/env bash

set -o errexit      # always exit on error
set -o pipefail     # do not ignore exit codes when piping output
set -o nounset      #exit if unset variables are used

cluster_name=$1
endpoint=$2
cluster_ca_certificate=$3

echo "generating static kubeconfig file for ${cluster_name}"

cat > kubeconfig.yaml <<EOF
apiVersion: v1
kind: Config
current-context: $(eval "echo ${cluster_name}")
contexts: [{name: $(eval "echo ${cluster_name}"), context: {cluster: $(eval "echo ${cluster_name}"), user: $(eval "echo ${cluster_name}")}}]
users: [{name: $(eval "echo ${cluster_name}"), user: {auth-provider: {name: gcp}}}]
clusters:
- name: $(eval "echo ${cluster_name}")
  cluster:
    server: "https://$(eval "echo ${endpoint}")"
    certificate-authority-data: "$(eval "echo ${cluster_ca_certificate}")"
EOF
