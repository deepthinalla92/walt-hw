variable "org_name" {
  type = string
  default = "deepthinalla"
}

variable "org_domain" {
  type = string
  default = "deepthinalla.com"
}

variable "k8sk8s_domain" {
  type = string
  default = "k8sk8s.com"
}

variable "org_id" {
  type = string
  default = "949998609106"
}

variable "primary_account_owner_id" {
  type = string
  default = "admin@deepthinalla.com"
}

variable "gcp_region" {
  type = string
  default = "us-west2"
}

variable "gcp_zone" {
  type = string
  default = "us-west2-a"
}

variable "bootstrap_project" {
  type = string
  default = "account-bootstrap"
}

variable "bootstrap_project_id" {
  type = string
  default = "account-bootstrap-289600"
}

variable "bootstrap_svc_account_id" {
  type = string
  default = "bootstrap-admin"
}

variable "creds_svc_accnt_bootstrap_admin" {
  type = string
  default = "/var/deepthinalla/gcp/creds/svc_acnt_bootstrap_admin.json"
}

variable "billing_account_id" {
  type = string
  default = "017E48-2C231E-80BF09"
}
