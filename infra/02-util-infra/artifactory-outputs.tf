output "gcp-artifact_registry_repo-org_docker_repo" {
  value = google_artifact_registry_repository.org_docker_repo
  description = "org docker repo in artifact registry"
}

output "gcp-svc_acc-artifact_registry_reader" {
  value = google_service_account.artifact_registry_reader
  description = "service account with read permissions to artifact registry"
}

output "gcp-svc_acc_iam-artifact_registry_reader_docker_repo_iam" {
  value = google_artifact_registry_repository_iam_member.artifact_registry_reader_docker_repo_iam
  description = "docker repository iam binding to artifact registry reader service account"
}

output "gcp-svc_acc-artifact_registry_writer" {
  value = google_service_account.artifact_registry_writer
  description = "service account with write permissions to artifact registry"
}

output "gcp-svc_acc_iam-artifact_registry_writer_docker_repo_iam" {
  value = google_artifact_registry_repository_iam_member.artifact_registry_writer_docker_repo_iam
  description = "docker repository iam binding to artifact registry writer service account"
}

output "gcp-svc_acc_key-artifact_registry_reader_key" {
  value = google_service_account_key.artifact_registry_reader_key
  description = "artifact registry reader svc acc key"
}

output "gcp-svc_acc_key-artifact_registry_writer_key" {
  value = google_service_account_key.artifact_registry_writer_key
  description = "artifact registry writer svc acc key"
}

output "gcp-gcs_bkt-helm_repo" {
  description = "gcs storage bucket for helm private repo"
  value = google_storage_bucket.helm_repo
}
