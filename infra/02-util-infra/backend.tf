terraform {
  backend "gcs" {
    credentials = "/var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
    bucket = "deepthinalla-org-tf-bknd"
    prefix = "util-infra"
  }
}
