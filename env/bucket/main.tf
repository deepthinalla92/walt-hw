resource "google_storage_bucket" "terragrunt_sample_resource" {
  name          = var.bucket_name
  location      = var.bucket_location
  force_destroy = true
}

output "bucket_terragrunt_sample_resource" {
    value = google_storage_bucket.terragrunt_sample_resource
}

variable "bucket_name" {
  description = "sample gcs bucket name"
  type = string
} 

variable "bucket_location" {
  description = "sample gcs bucket location"
  type = string
} 

variable "org_name" {
  type = string
  // default = "deepthinalla"
}

variable "org_domain" {
  type = string
  // default = "deepthinalla.com"
}

variable "org_id" {
  type = string
  // default = "949998609106"
}

variable "primary_account_owner_id" {
  type = string
  // default = "admin@deepthinalla.com"
}

variable "gcp_region" {
  type = string
  // default = "us-west2"
}

variable "gcp_zone" {
  type = string
  // default = "us-west2-a"
}

variable "bootstrap_project" {
  type = string
  // default = "account-bootstrap"
}

variable "bootstrap_project_id" {
  type = string
  // default = "account-bootstrap-289600"
}

variable "bootstrap_svc_account_id" {
  type = string
  // default = "bootstrap-admin"
}

variable "creds_svc_accnt_bootstrap_admin" {
  type = string
  // default = "/Users/deepthinalla/gdrive/deepthi/work/deepthinalla/gcp/creds/svc_accnt-account_bootstrap.json"
}

variable "svc_acnt_tf_bknd_creds" {
  type = string
}


variable "billing_account_id" {
  type = string
  // default = "017E48-2C231E-80BF09"
}

variable "env" {
  type = string
}