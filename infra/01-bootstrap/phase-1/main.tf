resource "random_id" "random_project_id_suffix" {
  byte_length = 2
}

resource "google_project" "util" {
  name = "util"
  billing_account = var.billing_account_id
  org_id = var.org_id
  project_id = format(
  "%s-%s",
  "util",
  random_id.random_project_id_suffix.hex
  )
}

resource "google_service_account" "tf_bknd" {
  account_id = "tf-bknd"
  project = google_project.util.project_id
}

resource "google_storage_bucket" "tf_bknd" {
  name = "${var.org_name}-${var.tf_bknd_bkt_name}"
  location = "US"
  project = google_project.util.number
}

resource "google_storage_bucket_iam_binding" "svc_acnt_tf_bknd_to_tf_bknd_bkt" {
  depends_on = [
    google_storage_bucket.tf_bknd,
    google_service_account.tf_bknd]

  bucket = "${var.org_name}-${var.tf_bknd_bkt_name}"
  role = "roles/storage.admin"

  members = [
    "serviceAccount:${google_service_account.tf_bknd.email}",
  ]
}

resource "google_service_account_key" "svc_acnt_tf_bknd" {
  service_account_id = google_service_account.tf_bknd.name
}
