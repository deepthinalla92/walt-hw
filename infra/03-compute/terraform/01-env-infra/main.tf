data "terraform_remote_state" "utl_infra" {
  backend = "gcs" 
  config = {
    credentials = "/var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
    bucket  = "deepthinalla-org-tf-bknd"
    prefix  = "util-infra"
  }
}

resource "random_id" "random_project_id_suffix" {
  byte_length = 3
}

resource "google_project" "compute" {
  name = "compute"
  billing_account = var.billing_account_id
  project_id = format(
    "%s-%s",
    "compute",
    random_id.random_project_id_suffix.hex
  )
  org_id = var.org_id
}

resource "google_project_service" "compute_project" {
  for_each = toset(var.compute_project_services)
  project = google_project.compute.project_id
  service = each.key

  disable_dependent_services = true
}

resource "google_service_account" "dns_01_solver" {
  description = "service account used for solving dns challenges while issuing certificates"
  account_id = "dns-01-solver"
  project = google_project.compute.project_id
}

resource "google_project_iam_member" "dns_01_solver_role_binding" {
  project = var.util_project_id
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.dns_01_solver.email}"
}

resource "google_service_account_key" "dns_01_solver_key" {
  service_account_id = google_service_account.dns_01_solver.name
}

resource "google_compute_network" "env_vpc" {
  depends_on = [google_project_service.compute_project]
  name = "compute-vpc"
  auto_create_subnetworks = false
  project = google_project.compute.project_id
}

resource "google_compute_subnetwork" "env_vpc_subnet" {
  name          = "compute-vpc-subnet"
  ip_cidr_range = "10.0.0.0/9"
  region        = var.gcp_region
  network       = google_compute_network.env_vpc.self_link
  private_ip_google_access = true
  project = google_project.compute.project_id
}

//#this is need for downloading docker images from artifact registry
//resource "google_compute_router" "env_router" {
//  name = "network-router"
//  network = google_compute_network.env_vpc.self_link
//  region  = var.gcp_region
//  project = google_project.compute.project_id
//}
//
//resource "google_compute_router_nat" "env_router_nat" {
//  name = "network-router-nat"
//  router = google_compute_router.env_router.name
//  project = google_project.compute.project_id
//  nat_ip_allocate_option = "AUTO_ONLY"
//  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
//}
