function echoHashBanner() {
    symbol="#########"
    echo "${symbol} ${*} ${symbol}"
}

function echoStarBanner() {
    symbol="*********"
    echo "${symbol} ${*} ${symbol}"
}

function echoAtBanner() {
    symbol="@@@@@@@@@"
    echo "${symbol} ${*} ${symbol}"
}

function echoInfo() {
    echo ""
    echoHashBanner "info: ${*}"
    echo ""
}

function echoWarning() {
    echo ""
    echoStarBanner "warning: ${*}"
    echo ""
}

function echoError() {
    echo ""
    echoAtBanner "error: ${*}"
    echo ""
}
