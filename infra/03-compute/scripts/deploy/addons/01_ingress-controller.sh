#!/usr/bin/env bash

set -o errexit  # always exit on error
set -o pipefail # do not ignore exit codes when piping output
set -o nounset  # exit if unset variables are used

source "${infra_compute_root}"/scripts/lib/echo.sh

ingress_lb_ip=${1}

echoInfo "installing addon: ingress-nginx"
echoInfo "creating ingress-nginx namespace"
kubectl create namespace ingress-nginx --dry-run=client -o yaml | kubectl apply -f -
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm upgrade --install nginx-ingress stable/nginx-ingress \
  --version 1.26.1 \
  -n ingress-nginx \
  --set controller.service.loadBalancerIP="${ingress_lb_ip}" \
  -f "${infra_compute_root}"/manifests/helm-vals-ingress.yaml \
  --dry-run="${dry_run}" --debug="${debug}" --wait
