provider "google" {
  region      = var.gcp_region
  credentials = var.creds_svc_accnt_bootstrap_admin
  version = "~> 3.39.0"
}