terraform {
  backend "gcs" {
    bucket="deepthinalla-org-tf-bknd"
    prefix="compute/cluster-infra"
    credentials="/var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
  }
}