output "gcp-prjct-util" {
  value = google_project.util
}

output "gcp-iam_svc_accnt-tf_bknd" {
  value = google_service_account.tf_bknd
}

output "gcp-storage_bkt-tf_bknd" {
  value = google_storage_bucket.tf_bknd
}

output "gcp-iam_storage_bkt_bndng-svc_acnt_tf_bknd_to_tf_bknd_bkt" {
  value = google_storage_bucket_iam_binding.svc_acnt_tf_bknd_to_tf_bknd_bkt
}

output "gcp-svc_acnt_key-svc_acnt_tf_bknd" {
  description = "private key json for svc_acnt_tf_bknd"
  value = google_service_account_key.svc_acnt_tf_bknd
  sensitive = true
}
