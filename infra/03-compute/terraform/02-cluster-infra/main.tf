data "terraform_remote_state" "utl_infra" {
  backend = "gcs"
  config = {
    credentials = "/var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
    bucket  = "deepthinalla-org-tf-bknd"
    prefix  = "util-infra"
  }
}

data "terraform_remote_state" "compute_env_infra" {
  backend = "gcs"
  config = {
    credentials = "/var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json"
    bucket  = "deepthinalla-org-tf-bknd"
    prefix  = "compute/env-infra"
  }
}

resource "google_container_cluster" "compute_cluster" {
  provider = google-beta
  name     = "compute"
  location = var.gcp_zone
  project = data.terraform_remote_state.compute_env_infra.outputs.gcp-prjct-compute.project_id
  network =  data.terraform_remote_state.compute_env_infra.outputs.gcp-nw_vpc-env_vpc.self_link
  subnetwork =  data.terraform_remote_state.compute_env_infra.outputs.gcp-nw_sn-env_vpc_subnet.self_link
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1
  release_channel {
    channel = "REGULAR"
  }
  addons_config {
    http_load_balancing {
      disabled = true
    }

    horizontal_pod_autoscaling {
      disabled = var.workers_scaling_hpa_enabled ? false : true
    }

    network_policy_config {
      disabled = var.network_policies_enabled ? false : true
    }
  }
  master_auth {
      username = ""
      password = ""
      client_certificate_config {
        issue_client_certificate = false
      }
  }
  private_cluster_config {
    enable_private_nodes = true
    enable_private_endpoint = false
    master_ipv4_cidr_block  = "172.16.0.0/28"
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block = ""
    services_ipv4_cidr_block = ""
  }

  master_authorized_networks_config {
    cidr_blocks {
      cidr_block   = "0.0.0.0/0"
      display_name = "all-for-testing"
    }
  }
}

resource "google_container_node_pool" "compute_cluster_node_pool_1" {
  provider = google-beta
  name       = "compute-np-1"
  location   = var.gcp_zone
  project = data.terraform_remote_state.compute_env_infra.outputs.gcp-prjct-compute.project_id
  cluster    = google_container_cluster.compute_cluster.name
  node_count = var.workers_node.initial
  autoscaling {
    min_node_count = var.workers_node.min
    max_node_count = var.workers_node.max
  }
  node_config {
    preemptible  = var.workers_node_preemptible
    machine_type = var.workers_node_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",
    ]
  }

  upgrade_settings {
    max_surge       = 1
    max_unavailable = 1
  }

  # the below lifecycle is important to prevent node pool recreation because of config drift created by cluster autoscaler
  lifecycle {
    ignore_changes = [
      node_count,
    ]
  }
}

resource "google_compute_address" "env_lb_compute_cluster_ingress" {
  name        = "comp-cluster-ing-ip"
  region      = var.gcp_region
  project     = data.terraform_remote_state.compute_env_infra.outputs.gcp-prjct-compute.project_id
}

resource "google_dns_record_set" "ingress_ip_in_env_zone_cluster_specific" {
  count = length(var.env_list)

  name          = format("*.%s.%s.", var.env_list[count.index], var.ingress_domain)
  project       = var.util_project_id
  managed_zone  = format("%s-root-zone", split(".", var.ingress_domain)[0])
  type          = "A"
  ttl           = 100

  rrdatas       = [google_compute_address.env_lb_compute_cluster_ingress.address]
}
