## helm template command for testing

```
helm template rlt-test-master . \
    --set product=deepthinalla \
    --set appEnv=stage \
    --set appVersion=mr-1 \
    --set appCommitSha=12334 \
    --set internetFacing=true \
    --set appName=rlt-test
```
