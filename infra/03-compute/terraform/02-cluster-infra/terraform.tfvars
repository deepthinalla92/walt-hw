env_list = [
  "stage",
  "prod"
]
workers_node_type = "n1-standard-4"
workers_node = {
  initial = "1"
  min = "1"
  max = "2"
}
workers_node_preemptible = "false"
workers_scaling_hpa_enabled = "true"
network_policies_enabled = "true"
ingress_domain = "k8sk8s.com"
util_project_id = "util-ee07"
