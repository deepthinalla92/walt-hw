variable "util_project_id" {
  type = string
  default = "util-ee07"
}

variable "org_gsuite_mx_records" {
  type = list(string)
  default = [
    "1 ASPMX.L.GOOGLE.COM.",
    "5 ALT1.ASPMX.L.GOOGLE.COM.",
    "5 ALT2.ASPMX.L.GOOGLE.COM.",
    "10 ALT3.ASPMX.L.GOOGLE.COM.",
    "10 ALT4.ASPMX.L.GOOGLE.COM.",
  ]
}

variable "org_google_domain_verification_txt_record" {
  type = list(string)
  default = [
    "google-site-verification=r3CsI0ptAGdCxibrO0dtTNP5gSFLgVQ6MjOKYjIxByU",
  ]
}

variable "org_docker_repo_name" {
  type = string
  default = "org-docker"
}

variable "util_project_services" {
  type = list
  default = [
    "dns.googleapis.com",
    "artifactregistry.googleapis.com",
  ]
}
