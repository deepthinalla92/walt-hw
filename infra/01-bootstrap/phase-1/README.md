# svc_acnt_tf_bknd

The service account key created to access terraform backend should be fetched from the tf outputs using the below commands.

```
pushd infra/bootstrap/phase-1
terraform output -json  gcp-svc_acnt_key-svc_acnt_tf_bknd | jq -r .private_key | base64 --decode > svc_acnt_tf_bknd.json
sudo mkdir -p /var/deepthinalla/gcp/creds
sudo cp svc_acnt_tf_bknd.json /var/deepthinalla/gcp/creds/svc_acnt_tf_bknd.json
popd
```
