output "gcp-ntwrk_ip_addr-env_ing_lb" {
  description = "ip address for the environment ingress load balancer"
  value = google_compute_address.env_lb_compute_cluster_ingress
}

output "gcp-gke_cluster-env_compute" {
  description = "compute cluster for the environment"
  value = google_container_cluster.compute_cluster
}

output "gcp-gke_node_pool-env_compute_np_1" {
  description = "node pool one for compute cluster for the environment"
  value = google_container_node_pool.compute_cluster_node_pool_1
}

output "gcp-ntwrk_dns_rec_set-env_ing_ip" {
  description = "dns record for environment ingress loadbalancer ip in env zone"
  value = google_dns_record_set.ingress_ip_in_env_zone_cluster_specific
}
