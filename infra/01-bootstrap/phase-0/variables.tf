variable "acnt_owner_roles" {
  type = list
  default = [
    "roles/resourcemanager.organizationAdmin",
    "roles/iam.securityAdmin",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/container.clusterAdmin",
    "roles/container.admin",
    "roles/compute.admin",
    "roles/dns.admin",
    "roles/compute.networkAdmin",
    "roles/billing.admin",
    "roles/storage.objectAdmin",
    "roles/monitoring.admin",
    "roles/artifactregistry.admin",
  ]
}

variable "svc_acnt_bootstrap_admin_roles" {
  type = list
  default = [
    "roles/billing.admin",
    "roles/serviceusage.serviceUsageAdmin",
    "roles/resourcemanager.projectCreator",
    "roles/resourcemanager.projectDeleter",
    "roles/storage.admin",
    "roles/iam.serviceAccountAdmin",
  ]
}

variable "bootstrap_project_services" {
  type = list
  default = [
    "cloudbilling.googleapis.com",
    "iam.googleapis.com",
    "storage-api.googleapis.com",
    "dns.googleapis.com",
    "artifactregistry.googleapis.com",
    "container.googleapis.com",
  ]
}
