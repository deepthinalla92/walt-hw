variable "env_list" {
  type = list(string)
}

variable "workers_node_type" {
  type = string
}

variable "workers_node" {
  type = object({
    initial = number
    min = number
    max = number
  })
}

variable "workers_node_preemptible" {
  type = bool
}

variable "workers_scaling_hpa_enabled" {
  type = bool
}

variable "network_policies_enabled" {
  type = bool
}

variable "ingress_domain" {
  type = string
}

variable "util_project_id" {
  type = string
}

